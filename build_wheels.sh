#!/bin/bash
set -e -x
shopt -s extglob

# make wheel with Python 3.9
PYBIN="/opt/python/cp39-cp39/bin/"
"${PYBIN}/pip" install -U pip --no-warn-script-location
"${PYBIN}/pip" install -q build
"${PYBIN}/python" -m build --wheel --outdir dist # wheelhouse

# Bundle external shared libraries into wheels
# for whl in wheelhouse/*.whl; do
#     auditwheel repair "$whl" -w dist
# done

# Install and test
cd test
for PYBIN in /opt/python/@(${PYVERS})*/bin; do
    "${PYBIN}/pip" install --prefer-binary -r ../test_requirements.txt
    "${PYBIN}/pip" install crumpets --no-index -f ../dist
    "${PYBIN}/python" -m pytest -vv
done
cd ..