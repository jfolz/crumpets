#!/usr/bin/env bash
set -e
cd doc

# use Python 3.9
PYBIN="/opt/python/cp39-cp39/bin/"

# install requirements
"${PYBIN}/pip" install -U pip --no-warn-script-location
"${PYBIN}/pip" install --prefer-binary -r ../doc_requirements.txt
"${PYBIN}/pip" install crumpets --no-index -f ../dist

# build doc
"${PYBIN}/python" -m sphinx -b html source out