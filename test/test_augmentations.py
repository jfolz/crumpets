import math

import numpy as np
import torch
import crumpets.torch.augmentation_cuda as cudaaugs

import crumpets.augmentation_cpu as cpuaugs


def test_noise_rgb_semantically_float():
    torch.manual_seed(1337)
    for strength in np.arange(0, 0.55, 0.05):
        N, C, H, W = (2, 3, 3000, 3000)
        im = (torch.zeros(N, C, H, W) + 0.5)
        expected_std = math.sqrt((2 * strength) ** 2 / 12)
        expected_mean = 0.5

        a = {'noise': strength}
        augs = [a] * N
        noisy = cudaaugs.add_noise_rgb(im, augs)
        mean = noisy.mean()
        std = noisy.transpose(0, 1).contiguous().view(C, -1).std(dim=1, unbiased=False)
        assert abs(std[0] - expected_std) <= 0.001
        assert abs(mean - expected_mean) <= 0.0001


def test_noise_rgb_semantically_byte():
    for strength in np.arange(0, 0.55, 0.05):
        N, C, H, W = (2, 3, 3000, 3000)
        im = (torch.zeros(N, C, H, W)).byte() + 2 ** 7
        expected_std = math.sqrt((2 * strength * (2 ** 8 - 1)) ** 2 / 12)
        expected_mean = 2 ** 7
        a = {'noise': strength}
        augs = [a] * N
        noisy = cudaaugs.add_noise_rgb(im, augs)
        mean = noisy.sum().float() / (N * C * H * W)
        std = ((noisy.float() - mean).pow(2).transpose(0, 1).contiguous().view(C, -1).sum(dim=1) / (N * H * W)).sqrt()
        assert abs(std[0] - expected_std) / (2 ** 8 - 1) <= 0.01
        assert abs(mean - expected_mean) / (2 ** 8 - 1) <= 0.01


def test_noise_other_semantically_float():
    torch.manual_seed(1337)
    for strength in np.arange(0, 0.55, 0.05):
        N, C, H, W = (2, 3, 3000, 3000)
        im = (torch.zeros(N, C, H, W) + 0.5)
        expected_std = math.sqrt((2 * strength) ** 2 / 12)
        expected_mean = 0.5

        a = {'noise': strength}
        augs = [a] * N
        noisy = cudaaugs.add_noise_other(im, augs)
        mean = noisy.mean()
        std = noisy.transpose(0, 1).contiguous().view(C, -1).std(dim=1, unbiased=False)
        assert abs(std[0] - expected_std) <= 0.001
        assert abs(mean - expected_mean) <= 0.0001


def test_noise_other_semantically_byte():
    for strength in np.arange(0, 0.55, 0.05):
        N, C, H, W = (2, 3, 3000, 3000)
        im = (torch.zeros(N, C, H, W)).byte() + 2 ** 7
        expected_std = math.sqrt((2 * strength * (2 ** 8 - 1)) ** 2 / 12)
        expected_mean = 2 ** 7
        a = {'noise': strength}
        augs = [a] * N
        noisy = cudaaugs.add_noise_other(im, augs)
        mean = noisy.sum().float() / (N * C * H * W)
        std = ((noisy.float() - mean).pow(2).transpose(0, 1).contiguous().view(C, -1).sum(dim=1) / (N * H * W)).sqrt()
        assert abs(std[0] - expected_std) / (2 ** 8 - 1) <= 0.01
        assert abs(mean - expected_mean) / (2 ** 8 - 1) <= 0.01


def _run_gamma(im, gamma_gray, gamma_color, contrast):
    augs = [{
        'color': True,
        'gamma_gray': gamma_gray,
        'gamma_color': [gamma_color] * 3,
        'contrast': contrast,
    }] * im.shape[0]
    gamma1 = cudaaugs.add_gamma(im, augs)
    gamma2 = torch.tensor(cpuaugs.add_gamma(
        im.squeeze().permute(1, 2, 0).numpy(),
        True,
        gamma_gray,
        [gamma_color] * 3,
        contrast,
    )).permute(2, 0, 1).unsqueeze(0)
    return gamma1, gamma2


def test_gamma_byte():
    for gamma_color in np.arange(0.6, 1.451, 0.3):
        for gamma_gray in np.arange(0.8, 1.251, 0.05):
            for contrast in np.arange(-1.0, 1.01, 0.05):
                im = torch.empty(1, 3, 300, 300, dtype=torch.uint8).random_()
                gamma1, gamma2 = _run_gamma(im, gamma_gray, gamma_color, contrast)
                diff = gamma1.to(torch.int16) - gamma2.to(torch.int16)
                err = diff.abs().max().item()
                assert err <= 1, (diff.min().item(), diff.max().item())


def test_gamma_short():
    for gamma_color in np.arange(0.6, 1.45, 0.05):
        for gamma_gray in np.arange(0.8, 1.25, 0.05):
            for contrast in np.arange(0, 1.01, 0.1):
                im = torch.empty(1, 3, 33, 79, dtype=torch.int16).random_()
                gamma1, gamma2 = _run_gamma(im, gamma_gray, gamma_color, contrast)
                diff = gamma1 - gamma2
                err = diff.abs().max().item()
                assert err <= 100, (diff.min().item(), diff.max().item())


def test_nogamma_float():
    im = torch.empty(500, 3, 100, 100).uniform_()
    a = {'color': True, 'gamma_gray': 1, 'gamma_color': [1] * 3, 'contrast': 0}
    result = cudaaugs.add_gamma(im, [a] * 500)
    diff = im - result
    err = diff.abs().max().item()
    assert err < 0.001


def test_blur():
    for sigma in np.arange(0.00005, 0.2, 0.008):
        im = torch.rand(1, 3, 500, 500)
        augs = [{'blur': sigma}]
        im_numpy = im.squeeze().transpose(0, 2).clone().numpy()
        blur1 = cudaaugs.add_blur(im, augs).squeeze()
        blur2 = torch.tensor(cpuaugs.add_blur(im_numpy, sigma)).transpose(0, 2)
        diff = blur1 - blur2
        err = diff.abs().mean().item()
        assert err < 0.01
