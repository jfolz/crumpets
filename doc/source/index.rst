.. Crumpets documentation master file, created by
   sphinx-quickstart on Fri Apr 26 15:12:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Crumpets - Delicious crumpets for training time
======================================================
Pre-process and augment your data, use training primitives for deep learning.

Below you can find the full documentation and a useful Quick Start Guide.


Quick Start Guide
==================
You are new to crumpets? Don't worry. We have prepared an easy to follow
guide for first steps. Learn about how to use crumpet's efficient data loading
utilities and training framework.

.. toctree::
   :maxdepth: 2

   quick_start_guide.rst


Data Augmentation
==================
Crumpets offers a great variety of data augmentation operations.
All those are integrated in crumpet's data loading pipeline, thus efficient and easy to use.
Here we present an overview of available augmentations and their usage.
On top of it you get the unique chance of seeing multiple pictures of sweet baby elephants!

.. toctree::
   :maxdepth: 2

   augmentation_guide.rst


Documentation
==================

.. toctree::
   :maxdepth: 2

   generated/crumpets.rst
   generated/crumpets.torch.rst
   generated/crumpets.workers.rst
   generated/crumpets.workers.saliency.rst
   generated/crumpets.workers.segmentation.rst


Examples
==================

.. toctree::
   :maxdepth: 2

   generated_examples/examples.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


Further Information
====================
Tough cookie.
