# Getting started with crumpets

We support Python 3.6+.

To build the main wheel:
`python setup.py bdist_wheel --dist-dir=.`

Torch bits are available as sub-package:
`python setup-torch.py bdist_wheel --dist-dir=.`

For now the easiest way to get started with crumpets are the examples.
E.g. `examples/dataloader_simple.py` shows how to create a dataloader from some iterable.
`examples/pytorch_cifar10.py` uses our other library *datadings* to load CIFAR10 and train a simple neural network.
